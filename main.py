# from databases import Database
from fastapi import FastAPI

from api import notes_router

# FastAPI app instance
app = FastAPI()

app.include_router(notes_router.router, prefix="/notes", tags=["notes management"])
