from pydantic import BaseModel


# Pydantic models for request and response validation
class NoteDetails(BaseModel):
    id: int
    title: str
    content: str


class NoteCreate(BaseModel):
    title: str
    content: str


class NoteUpdate(BaseModel):
    title: str
    content: str
