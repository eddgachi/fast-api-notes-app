## Git Basics

Git is a version control system used to track changes in source code during software development. It allows for efficient collaboration and management of project history.

### Key Concepts

- **Repository (Repo)**

  - A folder/directory where project files and version history are stored and managed by Git.

- **Commit**

  - Represents a snapshot of the project at a specific time, recording changes made since the last commit.

- **Branch**

  - An independent line of development allowing work on features/fixes without affecting the main project until ready to merge.

- **Remote**
  - A version of the repository hosted on a server (e.g., GitHub) enabling collaboration and backups.

### Basic Commands

```bash
# Initialize Repository
git init

# Add Files
git add <file>

# Commit Changes
git commit -m "Commit message"

# Check Status
git status

# View Commit History
git log

# Create Branch
git branch <branch-name>

# Switch Branch
git checkout <branch-name>

# Merge Branches
git merge <branch-name>

# Push Changes
git push <remote-name> <branch-name>

# Pull Changes
git pull <remote-name> <branch-name>
```

## Additional Tips

- ### Ignore Files

  - Use .gitignore file to specify files/directories Git should ignore.

- ### Resolve Conflicts

  - Manually resolve conflicts encountered when merging branches.

- ### Collaboration
  - Git enables simultaneous work by multiple developers on the same project.
