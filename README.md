# FastAPI Note-taking Application

This is a simple note-taking application built with FastAPI, SQLAlchemy, Alembic, and PostgreSQL. The application allows users to perform CRUD (Create, Read, Update, Delete) operations on notes stored in a PostgreSQL database.

## Features

- Create a new note with a title and content.
- Retrieve a specific note by ID.
- Update the title and content of an existing note.
- Delete a note by ID.

## Requirements

- Python 3.10+
- PostgreSQL
- `pip` (Python package installer)

## Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/eddgachi/fast-api-notes-app
```

2. Navigate to the project directory:

```bash
cd fast-api-notes-app
```

3. Install the required Python packages:

```bash
pip install -r requirements.txt
```

4. Set up the PostgreSQL database and configure the DATABASE_URL in main.py:

```python
DATABASE_URL = "postgresql://username:password@localhost/dbname"
```

5. Initialize the Alembic migration environment:

```bash
alembic init alembic
```

6. Modify the `alembic.ini` file in the `alembic` directory to point to your PostgreSQL database.

7. Generate the initial migration script and apply it to the database:

```python
alembic revision --autogenerate -m "initial"
alembic upgrade head
```

## Usage

1. Run the FastAPI application using Uvicorn:

```bash
uvicorn main:app --reload
```

2. Open your web browser and go to http://localhost:8000/docs to access the Swagger UI for interacting with the API.

## API Endpoints

- `GET /notes/all/`: Retrieve all notes.

- `POST /notes/`: Create a new note.

- `GET /notes/{note_id}`: Retrieve a note by ID.

- `PUT /notes/{note_id}`: Update a note by ID.

- `DELETE /notes/{note_id}`: Delete a note by ID.

## License

This project is licensed under the MIT License - see the LICENSE file for details.
