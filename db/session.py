from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

# SQLAlchemy database URL
# DATABASE_URL = "postgresql://username:password@localhost/dbname"
DATABASE_URL = "postgresql://postgres:S1r1_yangu@localhost/fast_api_notes_app"

# Database engine and session setup
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


# Dependency to get the database session
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
