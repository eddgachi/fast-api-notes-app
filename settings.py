{
  "workbench.iconTheme": "material-icon-theme",
  "editor.formatOnSave": true,
  "editor.codeActionsOnSave": {
    "source.organizeImports": "explicit"
  },
  "editor.suggestSelection": "first",
  "[python]": {
    "editor.defaultFormatter": "ms-python.black-formatter"
  },
  "python.languageServer": "Pylance",
  "python.analysis.typeCheckingMode": "off",
  "python.analysis.stubPath": "${workspaceFolder}/stubs",
  "files.associations": {
    "**/*.html": "html",
    "**/*.ts": "tsserver",
    // "*.vue": "Vue.volar",
    // "**/templates/partials/_sidebar.html": "esbenp.prettier-vscode",
    "**/templates/**/*.html": "django-html",
    "**/templates/**/*": "django-txt",
    "**/requirements{/**,*}.{txt,in}": "pip-requirements",
    "*.tpl": "django-html",
  },
  "emmet.includeLanguages": {
    "django-html": "html"
  },
  "emmet.triggerExpansionOnTab": true, // enable tab to expanse emmet tags
  "security.workspace.trust.untrustedFiles": "open",
  "files.exclude": {
    "**/.classpath": true,
    "**/.project": true,
    "**/.settings": true,
    "**/.factorypath": true
  },
  "files.autoSave": "onWindowChange",
  "extensions.ignoreRecommendations": true,
  "[vue]": {
    "editor.formatOnSave": true,
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "vetur.useWorkspaceDependencies": true,
  "vetur.format.defaultFormatter.scss": "prettier",
  "vetur.format.defaultFormatter.css": "prettier",
  "vetur.format.defaultFormatter.js": "prettier-eslint",
  "vetur.format.defaultFormatter.html": "prettier",
  "[dart]": {
    "editor.formatOnSave": true,
    "editor.formatOnType": true,
    "editor.rulers": [
      80
    ],
    "editor.selectionHighlight": false,
    "editor.suggest.snippetsPreventQuickSuggestions": false,
    "editor.suggestSelection": "first",
    "editor.tabCompletion": "onlySnippets",
    "editor.wordBasedSuggestions": "off"
  },
  "workbench.startupEditor": "none",
  "[jsonc]": {
    "editor.defaultFormatter": "vscode.json-language-features"
  },
  "[css]": {
    "editor.defaultFormatter": "vscode.css-language-features"
  },
  "[html]": {
    "editor.defaultFormatter": "vscode.html-language-features",
  },
  "prettier.singleQuote": true,
  "prettier.printWidth": 120,
  "prettier.semi": false,
  "editor.trimAutoWhitespace": false,
  "[php]": {
    "editor.formatOnSave": true,
    "editor.defaultFormatter": "bmewburn.vscode-intelephense-client"
  },
  "editor.fontFamily": "Fira Code iScript",
  "editor.tokenColorCustomizations": {
    "textMateRules": [
      {
        "scope": [
          // the following elements will be in italic
          //   (=Fira Code iScript)
          "comment",
          "keyword.control.import.js", // import
          "keyword.control.export.js", // export
          "keyword.control.from.js", // from
          // "constant", // String, Number, Boolean…, this, super
          "storage.modifier", // static keyword
          "storage.type.class", // class keyword
          "storage.type.php", // typehints in methods keyword
          "keyword.other.new.php", // new
          "entity.other.attribute-name", // html attributes
          "fenced_code.block.language.markdown" // markdown language modifier
        ],
        "settings": {
          "fontStyle": "italic"
        }
      },
      {
        "scope": [
          // the following elements will be displayed in bold
          "entity.name.type.class" // class names
        ],
        "settings": {
          "fontStyle": "italic"
        }
      },
      {
        "scope": [
          // the following elements will be displayed in bold and italic
          "entity.name.section.markdown" // markdown headlines
        ],
        "settings": {
          "fontStyle": "italic bold"
        }
      },
      {
        "scope": [
          // the following elements will be excluded from italics 
          //   (VSCode has some defaults for italics)
          "invalid",
          "keyword.operator",
          "constant.numeric.css",
          "keyword.other.unit.px.css",
          "constant.numeric.decimal.js",
          "constant.numeric.json",
          "comment.block",
          "entity.other.attribute-name.class.css"
        ],
        "settings": {
          "fontStyle": ""
        }
      }
    ]
  },
  "editor.unicodeHighlight.ambiguousCharacters": false,
  "cSpell.userWords": [
    "africas",
    "africastalking",
    "auditlog",
    "autoflush",
    "betsafe",
    "bodaboda",
    "cloudinary",
    "daraja",
    "Datatable",
    "daterange",
    "dependants",
    "dinga",
    "dingaworld",
    "dotenv",
    "fastapi",
    "freshdesk",
    "httpx",
    "iconnect",
    "ipay",
    "iterrows",
    "jasco",
    "joinedload",
    "jumuiya",
    "lipa",
    "mpayment",
    "mpayments",
    "mpesa",
    "nhif",
    "nssf",
    "occured",
    "openpyxl",
    "permissons",
    "Pesa",
    "pydantic",
    "rabbitmq",
    "REFERER",
    "relativedelta",
    "sessionmaker",
    "shortcode",
    "sqlalchemy",
    "starlette",
    "ussd",
    "uvicorn",
    "weasyprint",
    "yeastar"
  ],
  "git.autofetch": true,
  "dart.flutterSdkPath": "/home/frenchman/flutter",
  "[javascript]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "[json]": {
    "editor.defaultFormatter": "esbenp.prettier-vscode"
  },
  "liveServer.settings.donotShowInfoMsg": true,
  "editor.linkedEditing": true,
  "workbench.colorTheme": "One Dark Pro",
  "[django-html]": {
    "editor.defaultFormatter": "junstyle.vscode-django-support",
  },
  "editor.stickyScroll.enabled": false,
}
