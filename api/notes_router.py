from typing import List

from fastapi import APIRouter, Depends, HTTPException

from db.models.note import Note
from db.session import get_db
from schemas.notes_schema import NoteCreate, NoteDetails, NoteUpdate

router = APIRouter()


# Routes
@router.get("/all/", response_model=List[NoteDetails])
async def read_notes(skip: int = 0, limit: int = 100, db=Depends(get_db)):
    notes = db.query(Note).offset(skip).limit(limit).all()
    return notes


@router.post("/create", response_model=NoteDetails)
async def create_note(note: NoteCreate, db=Depends(get_db)):
    db_note = Note(**note.dict())
    db.add(db_note)
    db.commit()
    db.refresh(db_note)
    return db_note


@router.get("/details/{note_id}", response_model=NoteDetails)
async def read_note(note_id: int, db=Depends(get_db)):
    db_note = db.query(Note).filter(Note.id == note_id).first()
    if db_note is None:
        raise HTTPException(status_code=404, detail="Note not found")
    return db_note


@router.put("/update/{note_id}", response_model=NoteDetails)
async def update_note(note_id: int, note: NoteUpdate, db=Depends(get_db)):
    db_note = db.query(Note).filter(Note.id == note_id).first()
    if db_note is None:
        raise HTTPException(status_code=404, detail="Note not found")
    db_note.title = note.title
    db_note.content = note.content
    db.commit()
    db.refresh(db_note)
    return db_note


@router.delete("/delete/{note_id}")
async def delete_note(note_id: int, db=Depends(get_db)):
    db_note = db.query(Note).filter(Note.id == note_id).first()
    if db_note is None:
        raise HTTPException(status_code=404, detail="Note not found")
    db.delete(db_note)
    db.commit()
    return {"message": "Note deleted"}
